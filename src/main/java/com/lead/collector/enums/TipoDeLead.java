package com.lead.collector.enums;

public enum TipoDeLead {
    QUENTE,
    ORGANICO,
    FRIO,
}
