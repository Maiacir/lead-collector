package com.lead.collector.controllers;

import com.lead.collector.models.Lead;
import com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Lead")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping("/{indice}")
    public Lead BuscarLead(@PathVariable Integer indice){
        Lead lead = leadService.BuscarPorIndice(indice);
        return lead;
    }

    @PostMapping
    public Lead salvarLead(@RequestBody Lead lead){
        Lead leadObjeto = leadService.salvarLead(lead);
        return lead;
    }
}
