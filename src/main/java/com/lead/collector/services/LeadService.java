package com.lead.collector.services;

import com.lead.collector.enums.TipoDeLead;
import com.lead.collector.models.Lead;
import org.springframework.stereotype.Service;
import sun.nio.fs.GnomeFileTypeDetector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class LeadService {

    private List<Lead> leads = new ArrayList(Arrays.asList(
            new Lead("Carolina", "carolinamaia187@gmail.com", TipoDeLead.QUENTE)));

    public Lead BuscarPorIndice(int indice) {
        Lead lead = leads.get(indice);
        return lead;
    }

    public Lead salvarLead(Lead lead){
        leads.add(lead);
        return lead;

    }
}
